FROM alpine:3.9
RUN apk add --no-cache \
      bash \
      git \
      openssl
WORKDIR /app
COPY bin/* /bin/